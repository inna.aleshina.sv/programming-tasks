﻿

using System;
namespace VeryEasy1
{
    class Program
    {    //-----------------------------------------------TimeForMilkAndCookies----------------------------------------------- 
        //Christmas Eve is almost upon us, so naturally we need to prepare some milk and cookies for Santa! 
        //Create a function that accepts a Date object and returns true if it's Christmas Eve (December 24th) and false otherwise.
        //Working variant with boolean function:  
        //        public static bool TimeForMilkAndCookiesTrueFalse(int year, int month, int day)
        //        {
        //            return (month == 12 && day == 24) ? true : false;
        //        }

        //        // Main Method 
        //        public static void Main(string[] args)
        //        {
        //            int s1 = 12;

        //            // or declare String s2.Empty; 
        //            int s2 =24;
        //            int s3 = 12;

        //            bool b1 = TimeForMilkAndCookiesTrueFalse(s1, s2, s3);
        //            bool b2 = TimeForMilkAndCookiesTrueFalse(s2, s1, s3);
        //            bool b3 = TimeForMilkAndCookiesTrueFalse(s3, s2, s1);

        //            // same output as above program 
        //            Console.WriteLine(b1);
        //            Console.WriteLine(b2);
        //            Console.WriteLine(b3);
        //        }
        //    }
        //}

        //Working Interactive Variant 
        public static void TimeForMilkAndCookies ()
        {
            Console.WriteLine("--------------Task TimeForMilkAndCookies---------------- ");
            Console.WriteLine("enter day ");
            int day = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("enter month ");
          int month = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("enter year ");
            int year = Convert.ToInt32(Console.ReadLine());

          Console.WriteLine("so date is "+ day + "/ " + month + "/ " + year);


           if (day==24 && month==12)
          {
               Console.WriteLine("Hurrra! Christmas Eve ");            }
           else
           {
               Console.WriteLine("Not a Christmas Eve ((((((((");
           }
       }


        // ------------------------------Return the Next Number from the Integer Passed--------------------------------
        //Create a function that takes a number as an argument, increments the number by +1 and returns the result.
        
        public static void ReturnNextNumber()

        {
            Console.WriteLine("--------------Return the Next Number - 1st variant ---------------- ");
            Console.WriteLine("enter number ");
            int number = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"so, next number is    { (number+1)}");

        }

        // ------------------------------Return the Next Number 2d variant---------------------------------------------
        public static int ReturnNextNumber1()
       
        {int num = 0;
        Console.WriteLine("--------------Return the Next Number - 2d variant ---------------- ");
            Console.WriteLine("enter number ");
            num = Convert.ToInt32(Console.ReadLine());
            num++;
            Console.WriteLine("next number is " + num);
            return num;

        }
        //------------------------------Profitable gamble, common solution --------------------------------
        //bool profitableGamble(float prob, int prize, float pay)
        //{
        //    if (prob * prize > pay)
        //        return true;
        //    return false;
        //}

        //------------------------------Profitable gamble, Interactive mode--------------------------------
        //Create a function that takes in three arguments (prob, prize, pay) and returns true if prob * prize > pay; otherwise return false.
        public static void ProfitableGamble()
        {
            Console.WriteLine("--------------Profitable gamble ------------------------- ");
            Console.WriteLine("enter prob ");
            float prob = float.Parse(Console.ReadLine());

            Console.WriteLine("enter prize ");
            int prize = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("enter pay ");
            float pay = float.Parse(Console.ReadLine());

            Console.WriteLine("Lets see if it is profitable: ");


            if (prob * prize > pay)
            {
                Console.WriteLine("Hurrra! We are rich! ");
            }
            else
            {
                Console.WriteLine("Oh now, back to the mines, beggar ((((((((");
            }
        }

        //------------------------------Concatenate First and Last Name into One String, Interactive mode--------------------------------
        // Concatenate First and Last Name into One String
        //Given two strings, firstName and lastName, return a single string in the format "last, first".

        public static void MergeNames()
        {
            Console.WriteLine("--------------Profitable gamble ------------------------- ");
            Console.WriteLine("enter first name ");
            string first_name = Console.ReadLine();

            Console.WriteLine("enter last name ");
            string last_name = Console.ReadLine();
            string delimeter = ", ";

            //1st variant
            Console.WriteLine("Concatenation is: " + last_name + ", " + first_name);
            //2d variant
            string str = String.Concat(last_name, delimeter, first_name);
            Console.WriteLine("Another Concatenation is: " + str);
        }



        static void Main(string[] args)
       {

            TimeForMilkAndCookies();
            Console.WriteLine("--------------------------------------------------------");
            ReturnNextNumber();
            Console.WriteLine("--------------------------------------------------------");
            ReturnNextNumber1();
            Console.WriteLine("--------------------------------------------------------");
            ProfitableGamble();
            Console.WriteLine("--------------------------------------------------------");
            MergeNames();
            Console.WriteLine("--------------------------------------------------------");
        }


    }
}
