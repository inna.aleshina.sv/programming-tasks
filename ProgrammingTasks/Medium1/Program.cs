﻿//
// Remove All Special Characters from a String
//

using System;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Collections.Generic;


namespace Medium1
{


    class Program
    {
        //Create a function that accepts a string and returns true if it's in the format of a proper phone number and false if it's not. 
        //Assume any number between 0-9 (in the appropriate spots) will produce a valid phone number. This is what a valid phone number looks like: (123) 456-7890
        public static void IsPhoneNumberFormattedOK()
        {
            Console.WriteLine("--------------Is the Phone Number Formatted Correctly?---------------- ");
            Console.WriteLine("enter your phone number ");
            string phone_number = Console.ReadLine();
            Regex rgx = new Regex(@"^\(\d{3}\)\s\d{3}-\d{4}$");
            //string pattern = @"^\(\d{3}\) \d{3} - \d{4}$";
            if (rgx.IsMatch(phone_number))
                Console.WriteLine("your number " + phone_number + " is perfectly formatted ");
            else Console.WriteLine("your number " + phone_number + " is NOT formatted ");



        }


        //Create a function that takes an array of integers and removes the smallest value.
        //Don't change the order of the left over items.
        //If you get an empty array, return an empty array: [] ➞ [].
        //If there are multiple items with the same value, remove item with lower index(3rd example).

        public static void RemoveLowestValue()
        {
            Console.WriteLine("--------------Remove Lowest Value---------------- ");
            Console.WriteLine("enter your numbers ");
            string yourstring = Console.ReadLine();
            string[] separators = { ",", ".", "!", "?", ";", ":", " " };
            string[] numbers = yourstring.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            Console.WriteLine("Making int list from string ");
            int[] myInts = numbers.Select(int.Parse).ToArray();
           
            Console.WriteLine("min value is " + myInts.Min());

            List<int> temp = new List<int> (myInts);
            var query = temp.Where(x => x != temp.Min()).ToList();
            Console.WriteLine("New list is ");
            foreach (int c in query)
                Console.WriteLine(c);
        }





        //Basic email validation
        public static void BasicEmailValidation()
        {
            Console.WriteLine("--------------Basic email validation---------------- ");
            Console.WriteLine("enter your email ");
            string email = Console.ReadLine();
            Regex rgx = new Regex(@"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$");
           
            if (rgx.IsMatch(email))
                Console.WriteLine("your mail " + email + " is correct ");
            else Console.WriteLine("your mail " + email + " is NOT correct ");



        }



        //Create a function that takes an array of names and returns an array with the first letter capitalized.
        public static void CapitalizeTheNames()
        {
            Console.WriteLine("--------------Capitalize the Names---------------- ");
            Console.WriteLine("enter your name list ");
            string yourstring = Console.ReadLine();
            string[] separators = { ",", ".", "!", "?", ";", ":", " " };
            string[] values = yourstring.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            TextInfo ti = CultureInfo.InvariantCulture.TextInfo;
                foreach (var value in values)
                    Console.WriteLine("{0} --> {1}", value, ti.ToTitleCase(value.ToLower()));
            
        }


        //Check if the string is palindrome.
        //A palindrome is a word, phrase, number or other sequence of characters which reads the same backward or forward, such as madam or kayak.
        public static void CheckIfPalindrome()
        {
            Console.WriteLine("--------------Check if palindrome---------------- ");
            Console.WriteLine("enter your word or prase ");
            string yourstring = Console.ReadLine();
       
            yourstring = Regex.Replace(yourstring.ToLower(), "[^a-z]", "");
            
                  
            if ( yourstring.SequenceEqual(yourstring.Reverse()) is true)
                Console.WriteLine("Is a palindrome: " + yourstring); 
            else
                Console.WriteLine("Is NOT a palindrome: " + yourstring); 


        }

        //Create a function that takes a strings characters as ASCII and returns each characters hexadecimal value as a string.
        public static void ConvertToHex()
        {
            Console.WriteLine("--------------Convert to HEX---------------- ");
            Console.WriteLine("enter your word or prase ");
            string yourstring = Console.ReadLine();

            yourstring = string.Join(" ", yourstring.Select(chr => String.Format("{0:x2}", Convert.ToInt32(chr))));
            Console.WriteLine("HEX Output is:" + yourstring) ;



        }
    




        static void Main(string[] args)
        {
           IsPhoneNumberFormattedOK();
            Console.WriteLine("--------------------------------------------------------");
               RemoveLowestValue();
            Console.WriteLine("--------------------------------------------------------");
                 BasicEmailValidation();
            Console.WriteLine("--------------------------------------------------------");
              CapitalizeTheNames();
            Console.WriteLine("--------------------------------------------------------");
            CheckIfPalindrome();
            Console.WriteLine("--------------------------------------------------------");
            ConvertToHex();
            Console.WriteLine("--------------------------------------------------------");



        }


    }



    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Console.WriteLine(RemoveSpecialCharacters("The quick brown fox!") == "The quick brown fox");
    //        Console.WriteLine(RemoveSpecialCharacters("%fd76$fd(-)6GvKlO.") == "fd76fd-6GvKlO");
    //        Console.WriteLine(RemoveSpecialCharacters("D0n$c sed 0di0 du1") == "D0nc sed 0di0 du1");
    //    }

    //    public static string RemoveSpecialCharacters(string str)
    //    {
    //        return Regex.Replace(str, @"[^0-9a-zA-Z-_\s]+", "");
    //    }
    //}
}
