﻿
using System;
using System.Linq;  
using System.Text;  
using System.Collections.Generic;

namespace VeryEasy1
{
    class Program
    {    
        //Remove the First and Last Characters Interactive variant. If the string is 2 or fewer characters long, return the string itself.
        public static void RemoveFirstAndLastString ()
        {
            Console.WriteLine("--------------Remove the First and Last Characters---------------- ");
            Console.WriteLine("enter your string ");
            string yourstring=Console.ReadLine();
            string trimmedstr = yourstring.Substring(1, yourstring.Length - 2);
            int length = yourstring.Length;

            if ( length > 2 )
            {
              Console.WriteLine("Trimmed string is: " + trimmedstr);
                }
            else
                {
              Console.WriteLine("Cannot trim,  string is: " + yourstring);
                }
       }


        //Create a function replaces all the vowels in a string with a specified character.
        public static void ReplaceVowels ()
        {
            Console.WriteLine("--------------Replace Vowels---------------- ");
            Console.WriteLine("enter your string ");
            string yourstring=Console.ReadLine();

            Console.WriteLine("enter replace character ");
            var replacecharacter=Console.ReadLine();
            Console.WriteLine(System.Text.RegularExpressions.Regex.Replace(yourstring, "[aeiouAEIOU]", replacecharacter));
            


       }

        //Create a function that takes a string and returns a new string with all vowels removed.
        public static void RemoveVowels ()
        {
            Console.WriteLine("--------------Remove Vowels---------------- ");
            Console.WriteLine("enter your string ");
            string yourstring=Console.ReadLine();
    
            var letters = new HashSet < char > (yourstring);  
            letters.ExceptWith("AaEeIiOoUu");  
            Console.WriteLine("String after remove Vowels:");  
            foreach(char c in letters)  
            Console.Write(c);  
            Console.ReadLine();
           
       }
	   

        //Create a function that takes an array of strings. Return all words in the array that are exactly four letters.
        public static void Return4LettersString ()
        {
            Console.WriteLine("--------------Return 4 Letters String---------------- ");
            Console.WriteLine("enter your string ");
            string yourstring=Console.ReadLine();
            string[] separators = {",", ".", "!", "?", ";", ":", " "};
            string[] words = yourstring.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("Making list from string ");
            foreach (var word in words)
            Console.WriteLine(word);
            Console.WriteLine("------------------------------------------------------"); 
            Console.WriteLine("Lets find words with 4 letters only"); 
            for(var i = 0; i < words.Length; i++){
                 if(words[i].Length == 4)
                        Console.WriteLine(words[i]);
                  } 

         }
				     
        

        //Create a function that takes three integer arguments (a, b, c) and returns the number of equal values.

         public static void ReturnNumberOfEqualValues ()
        {
            Console.WriteLine("--------------Return Number Of Equal Values---------------- ");
            Console.WriteLine("enter your numbers ");
            string yourstring=Console.ReadLine();
            string[] separators = {",", ".", "!", "?", ";", ":", " "};
            string[] numbers = yourstring.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            int[] myInts = numbers.Select(int.Parse).ToArray();
			Console.WriteLine("Making int list from string ");
            foreach (var n in myInts)
            Console.WriteLine(n);
            Console.WriteLine("------------------------------------------------------"); 
            var count = numbers.GroupBy(item=>item).Where(item=>item.Count()>1).Count();
            Console.WriteLine("How many equal numbers do we have? Answer is: " + count); 
          
            
       
         }
      
     

        static void Main(string[] args)
       {

            RemoveFirstAndLastString ();
            Console.WriteLine("--------------------------------------------------------");
            ReplaceVowels ();
            Console.WriteLine("--------------------------------------------------------");
            RemoveVowels ();
            Console.WriteLine("--------------------------------------------------------");
            Return4LettersString ();
            Console.WriteLine("--------------------------------------------------------");
            ReturnNumberOfEqualValues ();
            Console.WriteLine("--------------------------------------------------------");
        }


    }
}
